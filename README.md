Písemka 2
================================================================

číslo π
----------------------------------------------------------------

Napište funkci pro výpočet čísla
[π](https://cs.wikipedia.org/wiki/Pí_%28číslo%29) pomocí součtu řady

![4/1 - 4/3 + 4/5 - 4/7 + ...](pi.svg)

```c
double cislo_pi(void) {
    double vysledek = 0;
    while () {
        ...
    }

    return vysledek;
}
```


Převod do binární soustavy
----------------------------------------------------------------

Napište funkci, která vytiskne číslo v binární soustavě. Nepoužívejte funkci
`printf` ale jen `putchar()`.


```c
void num2bin(int number) {
    unsigned int mask = 1 << 31;

    while (mask) {
        ...
        if () {
            purchar('\n');
        }
    }
}
```


Fibonacciho posloupnost
----------------------------------------------------------------

Napište funkci, která vypočítá n-tý člen [Fibonacciho
posloupnosti](https://cs.wikipedia.org/wiki/Fibonacciho_posloupnost):

![](fibonacci.svg)

```c
int fibonacci(int n) {
    ...
    return vysledek
}
```


Převodní tabulka
----------------------------------------------------------------

Vytvořte program, který na obrazovku vytiskne převodní tabulku ze stupňů Celsia
(od 0 do 100) na stupně Fahrenheita. Tabulka bude obsahovat i hlavičku s
nadpisem sloupců.

![](c2f.png)

příklad výstupu:

```
    C   |     F
---------------
    0°C |    32°F
    5°C |    41°F
   10°C |    50°F
   15°C |    59°F
   20°C |    68°F
   25°C |    77°F
   30°C |    86°F
   35°C |    95°F
   40°C |   104°F
   45°C |   113°F
   50°C |   122°F
   55°C |   131°F
   60°C |   140°F
   65°C |   149°F
   70°C |   158°F
   75°C |   167°F
   80°C |   176°F
   85°C |   185°F
   90°C |   194°F
   95°C |   203°F
  100°C |   212°F
```

Věk
----------------------------------------------------------------

Vytvořte funkci, které předáte rok narození a dle věku zařadí osobu do
odpovídající kategorie. Jméno kategorie se vytiskne. Aktuální rok v programu
uveďte jako symbolickou konstantu.

### Kategorie
* dítě (0-14 let)
* student (15-26 let)
* pracující (27-64 let)
* důchodce (65 a více let)

```c

#define ROK 2023

void vek(int rok_narozeni) {
    printf("dite\n");
    ....
    printf("duchodce\n");
}

int main(void) {
    ...
    vek(1981)
    ...
}
```


Největší společný násobek, nejmenší společný dělitel
----------------------------------------------------------------

Vytvořte funkci, která převezme  dvě celá kladná čísla a vrátí jejich
[největšího společného dělitele](https://cs.wikipedia.org/wiki/Nejv%C4%9Bt%C5%A1%C3%AD_spole%C4%8Dn%C3%BD_d%C4%9Blitel) 
a [nejmenší společný násobek](https://cs.wikipedia.org/wiki/Nejmen%C5%A1%C3%AD_spole%C4%8Dn%C3%BD_n%C3%A1sobek). 


```c
int nsn(int a, int b){
    int x,y;
    
    if (a > b) {
        x = 
    }

    do {
        x = ...
    } while ( x % y != 0)
    
    return cislo;
}

int nsd(int a, int b);
```


### nápověda:

NSN: Vezmu větší číslo a budu zkoušet jeho násobky, jestli jsou dělitelé tím druhým číslem.

NSD: Vezmu menší číslo a budu ho postupně zmenšovat dokud nenajdu číslo, kterým jsou 
`a`  i `b` dělitelné beze zbytku.


Prvočísla
----------------------------------------------------------------

Vytvořte funkci, která vytiskne všechna prvočísla ≤ N .

Vytvořte funkci, která vytiskne prvních N prvočísel.

### nápověda:

Udělejte si ne jednu, ale dvě funkce: jedna jen zjistí, jestli je zadané číslo provočíslo, 
druhá bude v cyklu procházet čísla a pokud bude aktuální číslo prvočíslem, vytiskne ho.

```c
int is_prime(int number){
    for (int i = 2; i <= number/2 ; i++) {
        if () return 0;
    }
    ...
}

void prime_numbers(int num) {
    for () {
        if (is_prime(i)) {
            ...
        }
    }
    putchar('\n');

}
```

Exponenciální funkce
----------------------------------------------------------------

Napište funkci pro výpočet [Exponenciální
funkce](https://cs.wikipedia.org/wiki/Exponenci%C3%A1ln%C3%AD_funkce) pomocí
mocninné řady:

![](ex.svg)

### nápověda:

Když se pozorně zadíváte na vzorec, zjistíte, že každý člen posloupnosti můžete vypočítat 
z toho předchozího členu tak, že ho vynásobíte `x` a vydělíte `n`. (`n` se postupně zvětšuje, vždy o jedna.)
